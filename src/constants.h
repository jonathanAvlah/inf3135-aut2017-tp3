// General constants
#define SCREEN_WIDTH  900
#define SCREEN_HEIGHT (900 * 4 / 5)
#define IMG_PATH "../assets/bravo.png"

// Menu constants
#define TITLE_FILENAME "../assets/bob.png"
#define TITLE_WIDTH 800
#define TITLE_X ((SCREEN_WIDTH - TITLE_WIDTH) / 2)
#define TITLE_Y 0
#define PLAY_FILENAME "../assets/play.png"
#define PLAY_WIDTH 600
#define PLAY_X ((SCREEN_WIDTH - PLAY_WIDTH) / 2)
#define PLAY_Y 370
#define QUIT_FILENAME "../assets/quit.png"
#define QUIT_WIDTH 600
#define QUIT_X ((SCREEN_WIDTH - QUIT_WIDTH) / 2)
#define QUIT_Y 500

// Maze
#define MAZE_INITIAL_X 0
#define MAZE_INITIAL_Y 600
#define MAZE_NUM_ROWS 6
#define MAZE_NUM_COLS 8

// Character
#define CHARACTER_SPRITESHEET "../assets/character.png"
#define CHARACTER_SCALE ((SCREEN_WIDTH) / 1050.0)
#define DONUTS_SPRITESHEET "../assets/donut.png"
#define DONUTS_SCALE ((SCREEN_WIDTH) / 1050.0)
#define MOVE_DURATION 450
#define DONUT_DURATION 60
#define BOB_JUMPING_VERTICALLY 3
#define BOB_WALKING_LEFT 1
#define BOB_COMING_DOWN 2
#define BOB_WALKING_RIGHT 0
#define BOB_JUMPING_RIGHT 4
#define BOB_JUMPING_LEFT 5
#define CHARACTER_BETWEEN_FRAME (MOVE_DURATION / 40)
#define CHARACTER_HORIZONTAL_STEP (SCREEN_WIDTH / 8)
#define CHARACTER_VERTICAL_STEP (SCREEN_HEIGHT / 6)

