#ifndef MAZE_H
#define MAZE_H

#include "utils.h"
#include "sdl2.h"
#include "donut.h"
#include <stdbool.h>

// --------------- //
// Data structures //
// --------------- //

struct Box {
    bool up;  // true if there is a wall at top
    bool left;
    bool down;
    bool right;
};

struct Maze {
    struct Position start;  // The starting position
    struct Position end;    // The end position
    SDL_Texture *texture;   // The texture (image)
    SDL_Renderer *renderer; // The renderer
    struct Box maze_matrix[6][8];
    struct Donut* donuts[3];
};

// --------- //
// Functions //
// --------- //

/**
 * Fill the matrix associated to the maze
 *
 * @param maze_matrix The matrix to fill
 */
void Maze_fillmatrix(struct Box maze_matrix[6][8]);

/**
 * Creates a maze from rectangles.
 *
 * @param renderer  The renderer
 * @return          A pointer to the create maze, NULL if there was an error;
 *                  Call IMG_GetError() for more information.
 */
struct Maze *Maze_create(SDL_Renderer *renderer);

/**
 * Delete the given maze.
 *
 * @param maze  The maze to be deleted
 */
void Maze_delete(struct Maze *maze);

/**
 * Renders the maze with a renderer.
 *
 * @param maze      The maze to be rendered
 */
void Maze_render(struct Maze *maze);

/**
 * Returns true if and only if there is a wall in the given direction.
 *
 * @param maze       The maze
 * @param position   The position
 * @param direction  The direction to look at
 * @return           true if and only if there is a wall
 */
bool Maze_hasWall(struct Maze *maze,
                  struct Position position,
                  enum Direction direction);

/**
 * @param donut      The donut
 * Returns true if the number of remaining donuts is equal to 3
 */
bool Maze_noMoreDonuts(struct Donut* donuts[3]);

#endif