#include "maze.h"
#include "constants.h"


/**
* Draws a SDL rectangle
*
* @param maze  The maze for which we are drawing rectangle
* @param x  The x coordinate of the rectangle
* @param y  The y coordinate of the rectangle
* @param w  The weight of the rectangle
* @param h  The height of the rectangle
*/
void Draw_rectangle(struct Maze *maze, int x, int y, int w, int h);

/**
* Generate the texture of the whole maze.
*
* @param maze  The maze whose texture is generated
*/
void Maze_generateTexture(struct Maze *maze);

/**
* Generates the donuts 
*
*
*  @param maze  The maze whose donuts is generated
*/
void Maze_generateDonuts(struct Maze *maze);

/**
*  If bob ate all the doughnuts.
*
*  @param   donuts[3]  table donuts.
* 
*  @return  true if bob ate all the doughnuts. 
*/
bool Maze_noMoreDonuts(struct Donut* donuts[3]);


// ---------------- //
// Public functions //
// ---------------- //


void Maze_fillmatrix(struct Box maze_matrix[6][8]) {
    for(int i=0; i<6; i++){
        for(int j=0; j<8; j++){
            maze_matrix[i][j].up = true;
            maze_matrix[i][j].down = true;
        }
   }
   maze_matrix[0][0].down = false;
   maze_matrix[0][5].down = false;
   maze_matrix[0][6].down = false;
   maze_matrix[0][7].down = false;
   maze_matrix[1][0].up = false;
   maze_matrix[1][5].up = false;
   maze_matrix[1][6].up = false;
   maze_matrix[1][7].up = false;
   maze_matrix[1][4].down = false;
   maze_matrix[1][5].down = false;
   maze_matrix[2][4].up = false;
   maze_matrix[2][5].up = false;
   maze_matrix[2][0].down = false;
   maze_matrix[2][5].down = false;
   maze_matrix[2][6].down = false;
   maze_matrix[3][0].up = false;
   maze_matrix[3][5].up = false;
   maze_matrix[3][6].up = false;
   maze_matrix[3][5].down = false;
   maze_matrix[3][7].down = false;
   maze_matrix[4][5].up = false;
   maze_matrix[4][7].up = false;
   maze_matrix[4][0].down = false;
   maze_matrix[4][6].down = false;
   maze_matrix[5][0].up = false;
   maze_matrix[5][6].up = false;

   for (int i=0; i<6; i++) {
       maze_matrix[i][0].left = true;
       maze_matrix[i][7].right = true; 
   }
  
   maze_matrix[0][6].right = true;
   maze_matrix[0][7].left = true;
   maze_matrix[1][4].right = true;
   maze_matrix[1][5].right = true;
   maze_matrix[1][5].left = true;
   maze_matrix[1][6].left = true;
   maze_matrix[2][4].right = true;
   maze_matrix[2][5].left = true;
   maze_matrix[3][5].right = true;
   maze_matrix[3][6].left = true;
   maze_matrix[4][5].right = true;
   maze_matrix[4][6].left = true;
}

struct Maze *Maze_create(SDL_Renderer *renderer) {
    struct Maze *maze;
    maze = (struct Maze*)malloc(sizeof(struct Maze));
    maze->renderer = renderer;
    Maze_fillmatrix(maze->maze_matrix); 
    Maze_generateTexture(maze);
    Maze_generateDonuts(maze);
    return maze;
}

void Maze_render(struct Maze *maze) {
    Maze_generateTexture(maze);
    for(int i=0;i<3;i++) {
        Donut_render(maze->donuts[i]);
    }
}

bool Maze_hasWall(struct Maze *maze,
                struct Position position,
                enum Direction direction) {
   struct Box box = maze->maze_matrix[position.row][position.col]; 
   switch (direction) {
       case DIRECTION_RIGHT:
           return box.right;
           break;
       case DIRECTION_UP:
           return box.up;
           break;
       case DIRECTION_LEFT:
           return box.left;
           break;
       case DIRECTION_DOWN:
           return box.down;
           break;
   }
}

void Maze_delete(struct Maze *maze) {
    if (maze != NULL) {
        if (maze->texture != NULL) {
            SDL_DestroyTexture(maze->texture);
            maze->texture = NULL;
        }
        free(maze);
    }
}

// ----------------- //
// Private functions //
// ----------------- //

void Draw_rectangle(struct Maze *maze,int x, int y, int w, int h){
    SDL_Rect r;
    r.x = x;
    r.y = y;
    r.w = w;
    r.h = h;
    SDL_SetRenderDrawColor(maze->renderer,0,128,128, 0);
    SDL_RenderFillRect(maze->renderer,&r);  
}

void Maze_generateTexture(struct Maze *maze) {
    SDL_SetRenderDrawColor(maze->renderer,255, 255, 255, 0);
    SDL_RenderClear(maze->renderer);

    Draw_rectangle(maze,0,0,SCREEN_WIDTH,10);
    Draw_rectangle(maze,0,710,SCREEN_WIDTH,10);
    Draw_rectangle(maze,0,0,10,SCREEN_HEIGHT);
    Draw_rectangle(maze,890,0,10,SCREEN_HEIGHT);
    Draw_rectangle(maze,103,110,460,10);
    Draw_rectangle(maze,10,230,460,10);
    Draw_rectangle(maze,103,350,460,10);
    Draw_rectangle(maze,10,470,550,10);
    Draw_rectangle(maze,103,590,572,10);
    Draw_rectangle(maze,778,0,10,120);
    Draw_rectangle(maze,553,110,10,240);
    Draw_rectangle(maze,665,350,10,240);
    Draw_rectangle(maze,665,470,110,10);
    Draw_rectangle(maze,665,120,10,113);
    Draw_rectangle(maze,665,230,225,10);
    Draw_rectangle(maze,780,350,113,10);
    Draw_rectangle(maze,780,590,113,10);
}

void Maze_generateDonuts(struct Maze *maze) {
    struct Point posScreen;
    struct Position posMaze;
    posScreen.x=450;
    posScreen.y=20;
    posMaze.row=0;
    posMaze.col=4;
    maze->donuts[0]=Donut_create(maze->renderer,&posScreen,&posMaze);
    AnimatedSpritesheet_render(maze->donuts[0]->animatedSpritesheet,
                                 maze->donuts[0]->screenPosition.x,
                                 maze->donuts[0]->screenPosition.y);

    posScreen.x=250;
    posScreen.y=250;
    posMaze.row=2;
    posMaze.col=2;
    maze->donuts[1]=Donut_create(maze->renderer,&posScreen,&posMaze);
    AnimatedSpritesheet_render(maze->donuts[1]->animatedSpritesheet,
                                 maze->donuts[1]->screenPosition.x,
                                 maze->donuts[1]->screenPosition.y);

    posScreen.x=780;
    posScreen.y=500;
    posMaze.row=4;
    posMaze.col=3;
    maze->donuts[2]=Donut_create(maze->renderer,&posScreen,&posMaze);
    AnimatedSpritesheet_render(maze->donuts[2]->animatedSpritesheet,
                                 maze->donuts[2]->screenPosition.x,
                                 maze->donuts[2]->screenPosition.y);

}
bool Maze_noMoreDonuts(struct Donut* donuts[3]){
    int cpt=0;
    for(int i=0;i<3;i++)
        if(donuts[i]->visited)
            cpt++;
    return cpt==3;
}
