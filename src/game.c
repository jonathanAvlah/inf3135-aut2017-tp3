#include "game.h"
#include "sdl2.h"
#include "constants.h"
#include <unistd.h>


// --------------------------- //
// Private function prototypes //
// --------------------------- //

bool Game_validMove(struct Game *game, enum Direction direction);

struct Game *Game_initialize(SDL_Renderer *renderer) {
    struct Game *game;
    game = (struct Game*)malloc(sizeof(struct Game));
    game->renderer = renderer;
    game->maze = Maze_create(renderer);
    game->character = Character_create(renderer);
    game->state = GAME_PLAY;
    return game;
}

void Game_delete(struct Game *game) {
    if (game != NULL) {
        Maze_delete(game->maze);
        Character_delete(game->character);
        free(game);
    }
}

void Game_run(struct Game *game) {
    SDL_Event e;
    while (game->state == GAME_PLAY) {
        if (SDL_PollEvent(&e) != 0) {
            if (e.type == SDL_QUIT) {
                game->state = GAME_QUIT;
            } else if (e.type == SDL_KEYDOWN) {
                switch (e.key.keysym.sym) {
                    case SDLK_LEFT:
                        if (Game_validMove(game, DIRECTION_LEFT)) {
                            Character_move(game->character,
                                           DIRECTION_LEFT,
                                           MOVE_DURATION);
                        }
                        break;
                    case SDLK_RIGHT:
                        if (Game_validMove(game, DIRECTION_RIGHT)) {
                            Character_move(game->character,
                                           DIRECTION_RIGHT,
                                           MOVE_DURATION);
                        }
                        break;
                    case SDLK_UP:
                        if (Game_validMove(game, DIRECTION_UP)) {
                            Character_move(game->character,
                                           DIRECTION_UP,
                                           MOVE_DURATION);
                        }
                        break;
                    case SDLK_DOWN:
                        if (Game_validMove(game, DIRECTION_DOWN)) {
                            Character_move(game->character,
                                           DIRECTION_DOWN,
                                           MOVE_DURATION);
                        }
                        break;
                    case SDLK_1:
                        if (Game_validMove(game, DIRECTION_UP)) {
                            Character_move(game->character,
                                           DIRECTION_UP_LEFT,
                                           MOVE_DURATION);
                            game->character->witness = -1;                  
                        }                        
                        break;
                    case SDLK_2:
                        if (Game_validMove(game, DIRECTION_UP)) {
                            Character_move(game->character,
                                           DIRECTION_UP_RIGHT,
                                           MOVE_DURATION);
                            game->character->witness = 1;
                        }
                        break;
                }
            }
        } else if(!game->character->moving) {
            if(game->character->witness == -1 && Game_validMove(game, DIRECTION_LEFT)) {
               Character_move(game->character,
                              DIRECTION_LEFT,
                              MOVE_DURATION);
            } else if(game->character->witness == 1 && Game_validMove(game, DIRECTION_RIGHT)) {
               Character_move(game->character,
                              DIRECTION_RIGHT,
                              MOVE_DURATION);            
            }
            game->character->witness = 0;
        }

        if (!game->character->moving) {
            struct Box box = game->maze->maze_matrix[game->character->mazePosition.row][game->character->mazePosition.col];
            if(!box.down) {
               Character_move(game->character,
                              DIRECTION_DOWN,
                              MOVE_DURATION);
            }
        }   
    
        
        if (Maze_noMoreDonuts(game->maze->donuts)) {
            game->state = GAME_MENU;
            SDL_SetRenderDrawColor(game->renderer, 0x00, 0x00, 0x00, 0x00 );
            SDL_RenderClear(game->renderer);
            // load our image
            SDL_Texture* img = IMG_LoadTexture(game->renderer, IMG_PATH);
            SDL_Rect texr;
            texr.x = 0;
            texr.y = 0;
            texr.w = SCREEN_WIDTH;
            texr.h = SCREEN_HEIGHT;
            SDL_RenderCopy(game->renderer, img, NULL, &texr);
            SDL_RenderPresent(game->renderer);
            sleep(4);
            SDL_DestroyTexture(img);
        } else {
            struct Position position = game->character->mazePosition;
            if (position.row == 0 && position.col == 4) {
                game->maze->donuts[0]->visited = true;
            }
            if (position.row == 2 && position.col == 2) {
                game->maze->donuts[1]->visited = true;
            }
            if (position.row == 4 && position.col == 7) {
                game->maze->donuts[2]->visited = true;
            }
            SDL_SetRenderDrawColor(game->renderer, 0x00, 0x00, 0x00, 0x00 );
            SDL_RenderClear(game->renderer);
            Maze_render(game->maze);
            Character_render(game->character);
            SDL_RenderPresent(game->renderer);
        }
    } 
}

bool Game_validMove(struct Game *game, enum Direction direction) {
    return !Maze_hasWall(game->maze, game->character->mazePosition, direction);
}
