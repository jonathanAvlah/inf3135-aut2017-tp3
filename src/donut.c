#include "constants.h"
#include "donut.h"

struct Donut * Donut_create(SDL_Renderer *renderer,struct Point *posScreen,struct Position *posMaze){
    struct Donut *donut;
    donut = (struct Donut *)malloc(sizeof(struct Donut));
    donut->renderer = renderer;
    donut->visited = false;
    donut->screenPosition.x=posScreen->x;
    donut->screenPosition.y=posScreen->y;
    donut->mazePosition.row = posMaze->row;
    donut->mazePosition.col = posMaze->col;
    donut->currentMove = 0;
    donut->animatedSpritesheet =
    AnimatedSpritesheet_create(DONUTS_SPRITESHEET, 1, 32, 32, DONUT_DURATION, renderer);
    donut->animatedSpritesheet->spritesheet->scale = DONUTS_SCALE;
    return donut;
}

void Donut_delete(struct Donut *donut) {
    if(donut != NULL) {
        AnimatedSpritesheet_delete(donut->animatedSpritesheet);
        free(donut);
    }
}

void Donut_render(struct Donut *donut) {
    if(!donut->visited) {
        if (donut->currentMove == 30) {
            donut->currentMove = 0;
        }
    AnimatedSpritesheet_setCol(donut->animatedSpritesheet,++donut->currentMove);
    AnimatedSpritesheet_render(donut->animatedSpritesheet,
                                 donut->screenPosition.x,
                                 donut->screenPosition.y);
    } else {
        donut->currentMove = 40;  
    }
}

