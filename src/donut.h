#ifndef DONUTS_H
#define DONUTS_H

#include "utils.h"
#include "maze.h"
#include "sdl2.h"
#include "animated_spritesheet.h"

// --------------- //
// Data structures //
// --------------- //

struct Donut{
	struct AnimatedSpritesheet *animatedSpritesheet; // His spritesheet .
 	struct Point screenPosition;                     // His position in the screen . 
 	struct Position mazePosition;                    // His position in the maze . 
 	bool visited;                                    // His visited .
	int currentMove;                                 // His current animation.   
 	SDL_Renderer *renderer;                          // The renderer .
};


// --------- //
// Functions //
// --------- //

/**
 * Creates the donut.
 *
 * @param renderer   The renderer .
 *        posScreen  The position in the screen .
 *        posMaze    The position in the maze .
 *
 * @return           A pointer to the donut , NULL if there was an error;
 *                   Call IMG_GetError() for more information.
 */
struct Donut * Donut_create(SDL_Renderer *renderer,struct Point *posScreen,struct Position *posMaze);

/**
 * Delete the donut.
 *
 * @param donut  The Donut to delete
 */
void Donut_delete(struct Donut *donut);

/**
 * Renders the donut.
 *
 * @param donut  The donut to render
 */
void Donut_render(struct Donut *donut);

#endif