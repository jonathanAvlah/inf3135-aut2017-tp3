# Travail pratique 3

## Description

Université du Quèbec à Montréal

Professeur : Alexandre Blondin-Massé

Cours : INF3135 Construction et maintenance de logiciels

Dans le cadre de la réalisation de notre troisième travail pratique, on 
avait une mission de développer un jeux video de plateformes. 


## Auteurs

- Jonathan Kodjo Avlah (AVLK10039703)
- Maher Kebaier        (KEBM07059103)
- Imad-Eddine ElHaimer (ELHI14099603)

## Fonctionnement

Une fois on a lancé le jeu, on aura donc le choix entre `1-Play` pour 
jouer ou `2-Quit` pour quitter. 
Puisque _Bob_ aime les beignets, le but du jeu consiste à lui permettre de les manger.
L'utilisateur peut faire  déplacer  _Bob_ avec les flèches `(droite, gauche)` et sauter à droite avec le bouton `2` et `1` pour sauter à gauche, et une fois _Bob_ aura mangé tout les beignets, une image  de félicitation s'affichera et le jeu retournera au `menu` principal. 


## Plateformes supportées

Le jeu a été testé sur Ubuntu 16.04.

## Dépendances

- [SDL2](https://www.libsdl.org/), une bibliothèque permettant de créer 
des applications multimédias qui comprennent du son et video.
Cette bibliothèque doit être installée sur le système pour que le projet fonctionne. 

Ainsi des packets qu'on a ajouté comme :

- libsdl2-gfx
- libsdl2-image
- libsdl2-ttf
- libsdl2-mixer



## Compilation

Pour compiler le jeu, il faut installer les dépendances nécessaires, cloner 
le projet et entrer la commande. 

~~~
make
~~~

Ensuite, la commande `./maze` nous affiche le `menu` du jeu.


## Références

- [Maze_sdl](https://bitbucket.org/ablondin-projects/maze-sdl), le jeu Maze du professeur.


## Division des tâches


- [X] Programmation du menu principal (Imed-Eddine)
- [X] Ajout du son au menu  principal (Maher)
- [X] Environement graphique  (Imed-Eddine et Jonathan)
- [X] Gestion des feuilles de sprites (Imed-Eddine et Jonathan)
- [X] Ajout écran félicitation (Maher)
- [X] Gestion des mouvements de Bob(Jonathan)
- [X] Bob mange les donuts (Maher)
- [X] Finalisation du code  (Maher)


## Statut

Actuellement, il y a un bug dans le logiciel, à une certaine exécution du programme il arrive que `Bob` refuse de se déplacer totalement ou partiellement. La fréquence à laquelle cela arrive varie d'une interface à une autre. 
L'option d'integration continue de _Gitlab_ ne fonctionne pas.
